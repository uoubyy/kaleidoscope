﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace Kaleidoscope
{
    class Ray : Entity
    {
        public bool enable = true;
        private Effect rayEffect;

        private Dictionary<string, Texture2D> rayTextures = new Dictionary<string, Texture2D>();
        private Dictionary<string, float> rayFloats = new Dictionary<string, float>();

        public Ray(string path, Vector3 pos, Matrix rot, bool _enable = true) : base(path, pos, rot)
        {
            scale = Matrix.CreateScale(0.2f, 0.2f, 0.2f);
            enable = _enable;
            rayEffect = Art.LoadEffect("FXs/Laser");

            rayTextures.Add("LaserBeamSampler", Art.LoadTexture("textures/laserBeamHomeMade"));
/*            rayTextures.Add("MaskTexture", Art.LoadTexture("textures/LaserBeam"));*/

            rayFloats.Add("TimeInSeconds", 0.0f);
        }

        public Vector3 GetDirection()
        {
            return worldPos + Vector3.Transform(new Vector3(-1, 0, 0), rotation);
        }

        public override void Update(GameTime gameTime)
        {
            rayFloats["TimeInSeconds"] = (float)gameTime.TotalGameTime.TotalSeconds;
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            //base.Draw(spriteBatch);
            if (enable)
            {
                DrawWithEffect(rayEffect, rayTextures, rayFloats);
            }
        }

        public void Move(Vector3 direction, float speed = 5.0f)
        {
            worldPos += direction * speed;
        }
    }
}

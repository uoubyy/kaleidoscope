﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Diagnostics;
using Microsoft.Xna.Framework.Media;

namespace Kaleidoscope
{
    class Laser
    {
        private Ray inRay;
        private Ray outRay;

        private Vector3 initPos;

        private float inDegree;
        private float outDegree;

        private bool hitted = false;
        private bool enable = false;

        private bool missed = false;

        public Laser(Vector3 position, Matrix rotation)
        {
            initPos = position;
            inRay = new Ray("models/new laser", position, rotation);
            outRay = new Ray("models/new laser reflected", position, rotation, false);
        }

        public void CalcReflect(Vector3 normal)
        {
            Vector3 inDir = -inRay.GetDirection();
            Vector3 outDir = Utils.Reflection(Vector3.Normalize(normal), Vector3.Normalize(inDir));

            inDegree = MathHelper.ToDegrees((float)Math.Atan2(inDir.Y, inDir.X)) % 360;
            outDegree = MathHelper.ToDegrees((float)Math.Atan2(outDir.Y, outDir.X)) % 360;
        }

        public virtual void Update(GameTime gameTime, Vector3 mirrorNormal)
        {
            if (enable == false)
                return;

            outRay.enable = true;
            if (inRay.worldPos.Z <= -5.0f)
                hitted = false;

            inRay.Move(new Vector3(0, 0, 1) * (float)gameTime.ElapsedGameTime.TotalSeconds);
            outRay.Move(new Vector3(0, 0, 1) * (float)gameTime.ElapsedGameTime.TotalSeconds);

            if (inRay.worldPos.Z <= 7.0f && hitted == false)
                CalcReflect(mirrorNormal);

            if (inRay.worldPos.Z > 7.0f && missed == false && hitted == false)
            {
                missed = true;
                MusicManager.Play(Art.OnMiss);
            }

            if (inRay.worldPos.Z <= -8.0f)
                outDegree = (inDegree + 180) % 360;
            else if (inRay.worldPos.Z > Consts.LaserStartZ + Consts.LaserDuration)
            {
                inRay.worldPos.Z = Consts.LaserStartZ;
                outRay.worldPos.Z = Consts.LaserStartZ;
                missed = false;
            }

#if DEBUG
            //Debug.WriteLine("InDegree {0}, OutDegree{1}", inDegree, outDegree);
#endif

            Matrix outRayRotation = Matrix.CreateRotationZ(MathHelper.ToRadians(outDegree));
            outRay.Rotate(outRayRotation);
        }

        public void Reset()
        {
            enable = false;
            inRay.worldPos.Z = Consts.LaserStartZ;
            outRay.worldPos.Z = Consts.LaserStartZ;
            missed = false;

            var rand = new Random();
            inRay.Rotate(Matrix.CreateRotationZ(MathHelper.ToRadians((float)rand.NextDouble() * 360.0f)));
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (enable == false)
                return;

            inRay.Draw(spriteBatch);
            outRay.Draw(spriteBatch);

            if (hitted == false)
            {
                if (outDegree - inDegree >= -2.0f && outDegree - inDegree <= 2.0f)
                {
                    hitted = true;
                    outDegree = inDegree;
                    Debug.WriteLine("Hitted {0} {1}", inDegree, outDegree);
                    GameEntry.GetInstance().OnHit();
                }
            }
            else
            {
                //Utils.DrawString(spriteBatch, new Vector2(40.0f, 100.0f),"Bingo! Go Next.");
            }
        }

        public void Enable(bool _enable)
        {
            enable = _enable;
        }

    }
}

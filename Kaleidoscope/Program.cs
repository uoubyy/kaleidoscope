﻿using System;

namespace Kaleidoscope
{
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            using (var game = GameEntry.GetInstance())
                game.Run();
        }
    }
}

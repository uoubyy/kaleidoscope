﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using System.Collections.Generic;

namespace Kaleidoscope
{
    class Entity
    {
        public string modelPath;
        public Vector3 worldPos;
        public Matrix rotation = Matrix.Identity;
        public Matrix scale = Matrix.Identity;

        protected Model model;

        public Entity(string path, Vector3 pos, Matrix rot)
        {
            modelPath = path;
            worldPos = pos;
            rotation = rot;

            model = Art.LoadModel(path);
        }

        public Entity(string path, Vector3 pos, Matrix rot, Matrix sca)
        {
            modelPath = path;
            worldPos = pos;
            rotation = rot;
            scale = sca;

            model = Art.LoadModel(path);
        }

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            DrawModel();
        }

        public virtual void Move(Vector3 offset)
        {
            worldPos += offset;
        }

        //public virtual void 

        public virtual void Scale(Matrix sca)
        {
            scale = sca;
        }

        public virtual void Scale(float sca)
        {
            scale = Matrix.CreateScale(sca);
        }

        public virtual void Rotate(Matrix rot)
        {
            rotation = rot;
        }

        protected void DrawModel()
        {
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.World = scale * rotation * Matrix.CreateTranslation(worldPos);
                    effect.View = Consts.ViewMatrix;
                    effect.Projection = Consts.ProjectMatrix;
                    effect.TextureEnabled = true;
                    //effect.EnableDefaultLighting();
                }
                mesh.Draw();
            }
        }

        public virtual void DrawWithEffect(Effect effect, Dictionary<string, Texture2D> textures, Dictionary<string, float> floats)
        {
            foreach (ModelMesh mesh in model.Meshes)
            {
                // Assign the appropriate values to each of the parameters
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = effect;
                    Matrix worldViewProjection = Matrix.Multiply(Matrix.Multiply(scale * rotation * Matrix.CreateTranslation(worldPos), Consts.ViewMatrix), Consts.ProjectMatrix);
                    part.Effect.Parameters["WorldViewProjection"].SetValue(worldViewProjection);

                    foreach (KeyValuePair<string, Texture2D> kvp in textures)
                        part.Effect.Parameters[kvp.Key].SetValue(kvp.Value);

                    foreach (KeyValuePair<string, float> kvp in floats)
                        part.Effect.Parameters[kvp.Key].SetValue(kvp.Value);
                }

                mesh.Draw();
            }

        }
    
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace Kaleidoscope
{
    class UButton
    {
        private string texName;
        private string texNamePressed;

        private Texture2D texture;
        private Texture2D texturePressed;

        private bool enable = true;

        private Vector2 position;

        private Vector2 origin;

        private Color color;

        private float lifeTime = float.PositiveInfinity;

        private Keys keyBind;
        private bool pressed = false;

        private float scale;

        public UButton(string _texName, Vector2 pos, Keys _keyBind, string __texNamePressed, float _scale)
        {
            texName = _texName;
            texNamePressed = __texNamePressed;
            position = pos;

            texture = Art.LoadTexture(texName);
            texturePressed = Art.LoadTexture(texNamePressed);

            origin = new Vector2(texture.Width / 2.0f, texture.Height / 2.0f);

            color = Color.White;
            keyBind = _keyBind;

            scale = _scale;
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (!enable)
                return;

            spriteBatch.Draw(pressed ? texturePressed : texture, position, null, new Color(color, 0.5f), 0, origin, scale, SpriteEffects.None, 1);
        }

        public virtual void Update(GameTime gameTime)
        {
            if (!enable)
                return;

            if (float.IsInfinity(lifeTime) == false)
            {
                lifeTime -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (lifeTime <= 0.0f)
                    enable = false;
            }

            if (Keyboard.GetState().IsKeyDown(keyBind))
                pressed = true;
            else
                pressed = false;
        }

        public virtual void Enable(bool _enable)
        {
            enable = _enable;
        }

        public void SetLifeTime(float _lifeTime)
        {
            lifeTime = _lifeTime;
        }
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Kaleidoscope
{
    class Utils
    {
        public static Vector2 GetProjectPoint(Vector3 vec)
        {
            Matrix mat = Consts.ViewMatrix * Consts.ProjectMatrix;

            Vector4 objectPos4 = new Vector4(vec, 1.0f);
            Vector4 postPerspectivePos = Vector4.Transform(objectPos4, mat);
            float clipSpaceX = postPerspectivePos.X / postPerspectivePos.W;
            float clipSpaceY = postPerspectivePos.Y / postPerspectivePos.W;

            return new Vector2((clipSpaceX + 1.0f) * Consts.ScreenWidth / 2.0f, (1.0f - clipSpaceY) * Consts.ScreenHeight / 2.0f);
        }

        public static Vector3 RandomInHemisphere(Vector3 normal)
        {
            var rand = new Random();
            float x = 0.3f * (float)rand.NextDouble();
            float y = 0.3f * (float)rand.NextDouble();
            float z = 0.3f * (float)rand.NextDouble();

            Vector3 randomDir = new Vector3(x, y, z);
            randomDir = Vector3.Normalize(randomDir);

            if (Vector3.Dot(randomDir, normal) > 0.0)
                return randomDir;
            else
                return -randomDir;
        }

        public static Vector3 Reflection(Vector3 normal, Vector3 inDir)
        {
            Vector3 nn = Vector3.Normalize(normal);
            Vector3 ni = Vector3.Normalize(inDir);

            Vector3 dir = ni + 2 * (Vector3.Dot(nn, -ni)) * nn;
            return dir;
        }
        public static void DrawLine3D(SpriteBatch spriteBatch, Vector3 from, Vector3 to, Color color, float thickness = 5f)
        {
            Vector2 start = GetProjectPoint(from);
            Vector2 end = GetProjectPoint(to);

            Vector2 delta = end - start;
            float length = delta.Length();
            float degree = (float)Math.Atan2(delta.Y, delta.X);

            spriteBatch.Draw(Art.Pixel, start, null, color, degree, new Vector2(0, 0), new Vector2(length, thickness), SpriteEffects.None, 0.0f);
        }

        public static void DrawCenteredString(SpriteBatch spriteBatch, string text)
        {
            var textWidth = Art.Font.MeasureString(text).X;
            var textHeight = Art.Font.MeasureString(text).Y;
            spriteBatch.DrawString(Art.Font, text, new Vector2(Consts.ScreenWidth / 2.0f - textWidth / 2.0f, Consts.ScreenHeight / 2.0f - textHeight / 2.0f - 200.0f), Color.Black);
        }

        public static void DrawString(SpriteBatch spriteBatch, Vector2 position, string text, float scale = 1.0f)
        {
            Vector2 offset = new Vector2(20, Art.Font.MeasureString(text).Y / 2 + 20);
            spriteBatch.DrawString(Art.Font, text, position + offset, Color.Black, 0.0f, Vector2.Zero, scale, SpriteEffects.None, 1.0f);
        }

    }
}

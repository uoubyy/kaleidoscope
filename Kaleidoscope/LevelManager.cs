﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;
using Microsoft.Xna.Framework.Media;

namespace Kaleidoscope
{
    public enum GameState
    {
        Idle,
        Run,
        LevelUp,
        GameOver
    }

    class LevelManager
    {
        private LaserManager laserManager = new LaserManager();
        private int level = 1;
        private int MaxLevel = 4;

        private int hitNum = 0;

        public GameState gameState = GameState.Idle;
        private double levelTime = 0.0f;

        private double bestRecord = double.PositiveInfinity;
        private double currentTime = 0.0f;

        private Entity[] tunnels = new Entity[3];
        private float tunnelRotate = 0.0f;

        private Entity hands;

        private UButton buttonLeft;
        private UButton buttonRight;

        private Effect tunnelEffect;
        private Dictionary<string, Texture2D> tunnelTextures = new Dictionary<string, Texture2D>();
        private Dictionary<string, float> tunnelFloats = new Dictionary<string, float>();

        private UText hitTip;

        public LevelManager()
        {
            laserManager.AddLaser(new Vector3(0, 0, Consts.LaserStartZ), Matrix.CreateRotationZ(MathHelper.ToRadians(120.0f)));
            laserManager.AddLaser(new Vector3(0, 0, Consts.LaserStartZ), Matrix.CreateRotationZ(MathHelper.ToRadians(60.0f)));
            laserManager.AddLaser(new Vector3(0, 0, Consts.LaserStartZ), Matrix.CreateRotationZ(MathHelper.ToRadians(150.0f)));
            laserManager.AddLaser(new Vector3(0, 0, Consts.LaserStartZ), Matrix.CreateRotationZ(MathHelper.ToRadians(30.0f)));

            tunnels[0] = new Entity("models/Kaleidospy_Tunnel", Vector3.Zero, Matrix.Identity, Matrix.CreateScale(3.0f));
            tunnels[1] = new Entity("models/Kaleidospy_Tunnel", new Vector3(0.0f, 0.0f, -20.0f), Matrix.Identity, Matrix.CreateScale(3.0f));
            tunnels[2] = new Entity("models/Kaleidospy_Tunnel", new Vector3(0.0f, 0.0f, -40.0f), Matrix.Identity, Matrix.CreateScale(3.0f));

            /*            tunnelEffect = Art.LoadEffect("FXs/TunnelFX");
                        tunnelTextures.Add("textSampler", Art.LoadTexture("textures/tile"));
                        tunnelFloats.Add("gametime", 0.0f);*/


            tunnelEffect = Art.LoadEffect("FXs/LaserFlow");
            tunnelTextures.Add("LaserBeamSampler", Art.LoadTexture("textures/TunnelWaveTexture"));
            tunnelFloats.Add("TimeInSeconds", 0.0f);

            hands = new Entity("models/Hands_main_camera", new Vector3(0f, -8f, -10f), Matrix.Identity);

            buttonLeft = new UButton("textures/Kaleidospy_button_Left", new Vector2(Consts.ScreenWidth / 2.0f - 220.0f, 100.0f), Keys.A, "textures/Kaleidospy_button_Left_lit", 0.2f);
            buttonRight = new UButton("textures/Kaleidospy_button_Right", new Vector2(Consts.ScreenWidth / 2.0f + 220.0f, 100.0f), Keys.D, "textures/Kaleidospy_button_Right_lit", 0.2f);

            hitTip = new UText("", new Vector2(Consts.ScreenWidth / 2.0f, Consts.ScreenHeight / 2.0f), 1.0f, "textures/blackBackgroundForText", 2.0f, false);
        }

        public void Update(GameTime gameTime, Vector3 mirrorNormal)
        {
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            //tunnelFloats["gametime"] = (float)gameTime.TotalGameTime.TotalSeconds;
            tunnelFloats["TimeInSeconds"] = (float)gameTime.TotalGameTime.TotalSeconds;

            foreach (Entity tunnel in tunnels)
            {
                if (tunnel.worldPos.Z >= 10.0f)
                    tunnel.worldPos.Z = -40.0f;
                tunnel.Move(new Vector3(0.0f, 0.0f, 1.0f * deltaTime));

                tunnelRotate = (tunnelRotate + 5.0f * deltaTime) % 360;
                Matrix rotation = Matrix.CreateRotationZ(MathHelper.ToRadians(tunnelRotate));
                tunnel.Rotate(rotation);
            }

            buttonLeft.Update(gameTime);
            buttonRight.Update(gameTime);

            if(level >= 2)
            {
                buttonLeft.Enable(false);
                buttonRight.Enable(false);
            }

            CalcLevel();

            if(gameState == GameState.Idle)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Space))
                    gameState = GameState.LevelUp;
            }
            else if (gameState == GameState.Run)
            {
                currentTime += gameTime.ElapsedGameTime.TotalSeconds;
                levelTime += gameTime.ElapsedGameTime.TotalSeconds;
                for(int i = 0; i < level; ++i)
                {
                    //Debug.WriteLine("Active idx {0}", (int)(levelTime / Consts.LaserInterval));
                    if (i <= (int)(levelTime / Consts.LaserInterval))
                    {
                        Laser laser = laserManager.GetLaser(i);
                        laser.Enable(true);
                        laser.Update(gameTime, mirrorNormal);
                    }
                }
            }
            else if(gameState == GameState.LevelUp)
            {
                levelTime += gameTime.ElapsedGameTime.TotalSeconds;
                if (levelTime >= 1.5f)
                {
                    gameState = GameState.Run;
                    buttonLeft.SetLifeTime(10.0f);
                    buttonRight.SetLifeTime(10.0f);
                    levelTime = 0;
                }
            }
            else if(gameState == GameState.GameOver)
            {
                if (currentTime <= bestRecord)
                    bestRecord = currentTime;
                if (Keyboard.GetState().IsKeyDown(Keys.R))
                {
                    gameState = GameState.LevelUp;
                    level = 1;
                    levelTime = 0;
                    laserManager.Reset();
                    hitNum = 0;
                    currentTime = 0;
                }
            }

            hitTip.Update(gameTime);
        }

        public void OnHit()
        {
            hitTip.SetText("Bingo!", 1.0f);
            MusicManager.Play(Art.OnHit);
            hitNum++;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach(Entity tunnel in tunnels)
            {
                tunnel.DrawWithEffect(tunnelEffect, tunnelTextures, tunnelFloats);
            }

            hands.Draw(spriteBatch);

            buttonLeft.Draw(spriteBatch);
            buttonRight.Draw(spriteBatch);

            if (gameState == GameState.GameOver)
            {
                Utils.DrawCenteredString(spriteBatch, "Congratulation!\n Esc to Quit.\nR to try again.");
            }
            else if(gameState == GameState.LevelUp)
            {
                if (level == 1)
                {
                    string message = string.Format("Level 1\nAlign Two Laser Beams Together \nTo Pass Through The Tunnel Safely");
                    Utils.DrawCenteredString(spriteBatch, message);
                }
                else
                {
                    Utils.DrawCenteredString(spriteBatch, string.Format("Level {0}", level));
                }
            }
            else if(gameState == GameState.Idle)
            {
                Utils.DrawCenteredString(spriteBatch, "Press Space to start");
            }
            else if (gameState == GameState.Run)
            {
                foreach (Laser laser in laserManager.GetTopNLasers(level))
                    laser.Draw(spriteBatch);
            }

            if(double.IsInfinity(bestRecord))
                Utils.DrawString(spriteBatch, Vector2.Zero, string.Format("Best record: NAN"), 0.6f);
            else
                Utils.DrawString(spriteBatch, Vector2.Zero, string.Format("Best record: {0:0.00}s", bestRecord), 0.6f);
            Utils.DrawString(spriteBatch, new Vector2(0, 40), string.Format("Current record: {0:0.00}s", currentTime), 0.6f);

            hitTip.Draw(spriteBatch);
        }

        public void CalcLevel()
        {
            if (hitNum >= level)
            {
                level++;
                gameState = GameState.LevelUp;
                levelTime = 0;
                laserManager.Reset();
                hitNum = 0;
            }

            if (level > MaxLevel)
                gameState = GameState.GameOver;
        }

        public bool IsGameOver()
        {
            return gameState == GameState.GameOver;
        }
    }
}

﻿using Microsoft.Xna.Framework;

namespace Kaleidoscope
{
    class Consts
    {
        public static int ScreenWidth = 1920;
        public static int ScreenHeight = 1080;

        public static Vector3 CameraPosition = new Vector3(0, 0, 10);

        public static Matrix ViewMatrix = Matrix.CreateLookAt(CameraPosition, new Vector3(0, 0, 0), Vector3.Up);
        public static Matrix ProjectMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(45.0f), ScreenWidth * 1.0f / ScreenHeight, 0.1f, 1000.0f);

        public static bool DebugON = false;

        public const float INFINITE = 10000f;

        public static float LaserStartZ = -15.0f;

        public static float LaserDuration = 30.0f;

        public static float LaserInterval = 15.0f;
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Media;

namespace Kaleidoscope
{
    class Art
    {
        private static Dictionary<string, Model> ModelPool = new Dictionary<string, Model>();
        private static Dictionary<string, Texture2D> TexturePool = new Dictionary<string, Texture2D>();
        public static Texture2D Pixel { get; private set; }
        public static Song BackMusic { get; private set; }
        public static Song OnHit { get; private set; }
        public static Song OnMiss { get; private set; }
        public static SpriteFont Font { get; private set; }
        public static Effect LaserEffect { get; private set; }
        public static Texture2D LaserMask { get; private set; }
        public static Texture2D LaserMain { get; private set; }


        public static void Load(ContentManager content)
        {
            BackMusic = content.Load<Song>("audio/Kaleidospy_MainTrack");
            OnHit = content.Load<Song>("audio/hit");
            OnMiss = content.Load<Song>("audio/laserDestroy");
            
            Font = content.Load<SpriteFont>("fonts/Delicious");

            LaserEffect = content.Load<Effect>("FXs/Laser");
            LaserMask = content.Load<Texture2D>("textures/GradientCenter");
            LaserMain = content.Load<Texture2D>("textures/LaserBeam");

            Pixel = new Texture2D(LaserMain.GraphicsDevice, 1, 1);
            Pixel.SetData(new[] { Color.White });
        }

        public static Model LoadModel(string path)
        {
            if (!ModelPool.ContainsKey(path))
            {
                Model model = GameEntry.GetInstance().Content.Load<Model>(path);
                ModelPool.Add(path, model);
            }
            return ModelPool[path];
        }

        public static Texture2D LoadTexture(string path)
        {
            if (!TexturePool.ContainsKey(path))
            {
                Texture2D texture = GameEntry.GetInstance().Content.Load<Texture2D>(path);
                TexturePool.Add(path, texture);
            }
            return TexturePool[path];
        }

        public static Effect LoadEffect(string path)
        {
            return GameEntry.GetInstance().Content.Load<Effect>(path);
        }
    }
}

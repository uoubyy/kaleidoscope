﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;

namespace Kaleidoscope
{
    class MusicManager
    {
        public static void Init()
        {
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(Art.BackMusic);
        }

        public static void Play(Song track)
        {
            MediaPlayer.IsRepeating = false;
            MediaPlayer.Play(track);
        }

        public static void Update()
        {
            if(MediaPlayer.State == MediaState.Stopped)
            {
                Init();
            }
        }
    }
}

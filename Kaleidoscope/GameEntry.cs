﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Kaleidoscope
{
    public class GameEntry : Game
    {
        public GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private static GameEntry _instance = null;

        private HexagonEntity hexagonEntity;
        private HexagonEntity hexagonEntitySmall;

        private LevelManager levelManager;

        public static GameEntry GetInstance()
        {
            if (_instance == null)
                _instance = new GameEntry();
            return _instance;
        }

        public GameEntry()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = false;
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            _graphics.PreferredBackBufferWidth = Consts.ScreenWidth;
            _graphics.PreferredBackBufferHeight = Consts.ScreenHeight;
            _graphics.ApplyChanges();

            levelManager = new LevelManager();

            hexagonEntity = new HexagonEntity("models/Kaleidospy_KaleidoscopeBall_M1", Vector3.Zero, Matrix.Identity);
            hexagonEntity.Scale(1.2f);
            hexagonEntitySmall = new HexagonEntity("models/Kaleidospy_KaleidoscopeBall_M1", new Vector3(0.0f, 0.0f, 3.0f), Matrix.Identity, -1.0f);
            hexagonEntitySmall.Scale(0.8f);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            Art.Load(Content);
            MusicManager.Init();

        }

        protected override void Update(GameTime gameTime)
        {
            MusicManager.Update();

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            Vector3 mirrorDir = hexagonEntity.GetMirrorNormal();
            levelManager.Update(gameTime, mirrorDir);

            if (GetGameState() == GameState.Run)
            {
                hexagonEntity.Update(gameTime);
                hexagonEntitySmall.Update(gameTime);
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);

            // TODO: Add your drawing code here
            _spriteBatch.Begin();

            levelManager.Draw(_spriteBatch);

            hexagonEntity.Draw(_spriteBatch);
            hexagonEntitySmall.Draw(_spriteBatch);
            //Vector3 mirrorDir = hexagonEntity.GetMirrorNormal();
            
            //Utils.DrawLine3D(_spriteBatch, Vector3.Zero - mirrorDir * 10.0f, Vector3.Zero + mirrorDir * 10.0f * 10.0f, Color.Red);
            //Utils.DrawLine3D(_spriteBatch, Vector3.Zero - new Vector3(50, 0, 0), Vector3.Zero + new Vector3(50, 0, 0), Color.Blue);

            _spriteBatch.End();

            base.Draw(gameTime);
        }

        public void OnHit()
        {
            levelManager.OnHit();
        }

        public GameState GetGameState()
        {
            return levelManager.gameState;
        }

    }
}


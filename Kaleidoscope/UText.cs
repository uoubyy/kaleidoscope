﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace Kaleidoscope
{
    class UText
    {
        private string text;

        private Texture2D textureBack;

        private bool enable = true;

        private float lifeTime = float.PositiveInfinity;

        private float scale = 1.0f;

        private Vector2 position;
        private Vector2 defaultPos;

        public UText(string _text, Vector2 _pos, float _scale, string _textureBack, float _lifeTime, bool _enable = true)
        {
            text = _text;
            textureBack = Art.LoadTexture(_textureBack);
            enable = _enable;
            scale = _scale;
            position = _pos;
            lifeTime = _lifeTime;

            defaultPos = _pos;
        }

        public void SetText(string _text, float _lifeTime, bool _enable = true)
        {
            text = _text;
            lifeTime = _lifeTime;
            enable = _enable;
            position = defaultPos;
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (!enable)
                return;

            var textWidth = Art.Font.MeasureString(text).X;
            var textHeight = Art.Font.MeasureString(text).Y;

            Vector2 backSize = new Vector2(textWidth + 20.0f, textHeight);
            Vector2 backScale = new Vector2(backSize.X / textureBack.Width, backSize.Y / textureBack.Height);
            Vector2 backOrigin = new Vector2(textureBack.Width / 2.0f, textureBack.Height / 2.0f);

            //(SpriteFont spriteFont, string text, Vector2 position, Color color, float rotation, Vector2 origin, Vector2 scale, SpriteEffects effects, float layerDepth);
            spriteBatch.Draw(textureBack, position - new Vector2(10f, 100.0f), null, Color.White, 0, backOrigin, backScale, SpriteEffects.None, 0);
            spriteBatch.DrawString(Art.Font, text, position + new Vector2(150f, 0.0f), Color.White, 0.0f, backOrigin, 1.0f, SpriteEffects.None, 1);
        }

        public virtual void Update(GameTime gameTime)
        {
            if (!enable)
                return;

            lifeTime -= (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (lifeTime <= 0)
                position.X += (float)gameTime.ElapsedGameTime.TotalSeconds * 350.0f;

            if (position.X >= Consts.ScreenWidth * 0.75f)
                enable = false;
        }

        public virtual void Enable(bool _enable)
        {
            enable = _enable;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Kaleidoscope
{
    class LaserManager
    {
        private List<Laser> lasers = new List<Laser>();

        public LaserManager()
        {

        }

        public void AddLaser(Vector3 position, Matrix rotation)
        {
            lasers.Add(new Laser(position, rotation));
        }

        public void Update(GameTime gameTime, Vector3 mirrorNormal)
        {
            foreach (Laser laser in lasers)
                laser.Update(gameTime, mirrorNormal);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Laser laser in lasers)
                laser.Draw(spriteBatch);
        }

        public List<Laser> GetTopNLasers(int num)
        {
            return lasers.GetRange(0, num);
        }

        public Laser GetLaser(int idx)
        {
            return lasers[idx];
        }

        public void Reset()
        {
            foreach (Laser laser in lasers)
                laser.Reset();
        }
    }
}

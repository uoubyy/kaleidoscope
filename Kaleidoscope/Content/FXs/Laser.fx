﻿#if OPENGL
#define SV_POSITION POSITION
#define VS_SHADERMODEL vs_3_0
#define PS_SHADERMODEL ps_3_0
#else
#define VS_SHADERMODEL vs_4_0_level_9_1
#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

matrix WorldViewProjection;
sampler2D LaserBeamSampler;
float TimeInSeconds;

struct VertexShaderInput
{
    float4 Position : POSITION0;
    float4 Color : COLOR0;
    float4 TextureCoordinate : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : SV_POSITION;
    float4 Color : COLOR0;
    float2 TextureCoordinate : TEXCOORD0;
};

VertexShaderOutput MainVS(in VertexShaderInput input)
{
    VertexShaderOutput output = (VertexShaderOutput) 0;

    output.Position = mul(input.Position, WorldViewProjection);
    output.Color = input.Color;
    output.TextureCoordinate = input.TextureCoordinate.xy;
	
    return output;
}

float4 MainPS(VertexShaderOutput input) : COLOR
{
    float2 UV = input.TextureCoordinate + TimeInSeconds * float2(0.2f, 1.0f);
    UV = float2(frac(UV.x), frac(UV.y));
    float4 col = tex2D(LaserBeamSampler, UV);

    float gamma = 1.0f / 2.2f;
    col.xyz = pow(abs(col.xyz), float3(gamma, gamma, gamma));
    return col;
}

technique BasicColorDrawing
{
    pass P0
    {
        VertexShader = compile VS_SHADERMODEL MainVS();
        PixelShader = compile PS_SHADERMODEL MainPS();
    }
};
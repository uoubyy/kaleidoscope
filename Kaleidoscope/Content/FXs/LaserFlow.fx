﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

matrix WorldViewProjection;
sampler2D LaserBeamSampler;
float TimeInSeconds;

struct VertexShaderInput
{
	float4 Position : POSITION0;
	float4 Color : COLOR0;
    float4 TextureCoordinate : TEXCOORD0;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR0;
    float2 TextureCoordinate : TEXCOORD0;
};

VertexShaderOutput MainVS(in VertexShaderInput input)
{
	VertexShaderOutput output = (VertexShaderOutput)0;

	output.Position = mul(input.Position, WorldViewProjection);
	output.Color = input.Color;
    output.TextureCoordinate = input.TextureCoordinate.xy;
	
	return output;
}

float4 MainPS(VertexShaderOutput input) : COLOR
{
    float2 UV = input.TextureCoordinate + TimeInSeconds * float2(0.2f, 1.0f);
    UV = float2(frac(UV.x), frac(UV.y));
    float4 col = tex2D(LaserBeamSampler, UV);
	
    float red = col.r * sin(TimeInSeconds) / 2 + 0.5f;
    float green = col.g * sin(TimeInSeconds + 2) / 2 + 0.5f;
    float blue = col.b * sin(TimeInSeconds + 4) / 2 + 0.5f;
	
    float3 fragColor = float3(red, green, blue);

    float gamma = 1.0f / 0.5f;
    fragColor = pow(abs(fragColor), float3(gamma, gamma, gamma));
    return float4(fragColor, col.a);
}

technique BasicColorDrawing
{
	pass P0
	{
		VertexShader = compile VS_SHADERMODEL MainVS();
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};
﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;

namespace Kaleidoscope
{
    class HexagonEntity : Entity
    {
        private float rotateDegree = 0.0f;
        private float rotateSpeed = 30.0f;
        private float dir;

        private Vector3 mirrorNormalDefault;

        public HexagonEntity(string path, Vector3 position, Matrix rotation, float _dir = 1.0f) : base(path, position, rotation)
        {
            mirrorNormalDefault = new Vector3(-1, 1, 0);
            dir = _dir;
        }

        public override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.A))
                rotateDegree += rotateSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds * dir;
            else if (Keyboard.GetState().IsKeyDown(Keys.D))
                rotateDegree -= rotateSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds * dir;

            rotateDegree = rotateDegree % 360.0f;
            rotation = Matrix.CreateRotationZ(MathHelper.ToRadians(rotateDegree));
        }

        public Vector3 GetMirrorNormal()
        {
            return Vector3.TransformNormal(mirrorNormalDefault, rotation); ;
        }
    }
}
